#!/bin/bash

echo name $NAME
echo zone $ZONE
echo record $RECORD

function get_public_ip() { 
	echo $(dig +short myip.opendns.com @resolver1.opendns.com) 
}

function update_record() {
	echo "Updating $1"
	curl -X "PUT" "https://dns.hetzner.com/api/v1/records/$RECORD" \
	     -H 'Content-Type: application/json' \
	     -H "Auth-API-Token: $TOKEN" \
	     -d "{ \
	  \"value\": \"$1\",
	  \"ttl\": 60,
	  \"type\": \"A\",
	  \"name\": \"$NAME\",
	  \"zone_id\": \"$ZONE\"
	}"
}

current_ip=""

while true
do
	resolved_ip=$(get_public_ip)
	if [ "$current_ip" != "$resolved_ip" ]; then
		update_record $resolved_ip
		current_ip=$resolved_ip
	fi
	sleep 60
done

